//
//  ProductGridVCTests.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import DishWashers

extension ProductGridVCTests {
    class MockProductGridViewModel: ProductGridViewModel {
        var getProductsCalled = false
        func getProducts() {
            getProductsCalled = true
        }
        func numberOfProducts() -> Int {
            return 0
        }
        func product(at index: Int) -> ProductViewModel? {
            return nil
        }
        func photo(at index: Int, completionHandler: @escaping (Result<UIImage>) -> Void) {
        }
    }
    
    class MockUICollectionView : UICollectionView {
        var reloadGotCalled = false
        override func reloadData() {
            reloadGotCalled = true
        }
    }
}

class ProductGridVCTests: XCTestCase {
    var sut: ProductGridVC!
    var mockViewModel: MockProductGridViewModel!
    
    override func setUp() {
        super.setUp()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = storyBoard.instantiateViewController(withIdentifier: "ProductGridVC") as! ProductGridVC
        mockViewModel = MockProductGridViewModel()
        sut.viewModel = mockViewModel
    }
    
    func testProductGridVCHasAViewModel() {
        // Assert
        XCTAssertNotNil(sut.viewModel)
    }
    
    func testProductsCollectionView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.productsCollectionView)
    }
    
    func testProductsCollectionView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.productsCollectionViewDataSource = ProductsCollectionViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert(sut.productsCollectionViewDataSource is ProductsCollectionViewDataSource)
    }
    
    func testViewLoad_shouldAskViewModelForProducts() {
        //Act
        _ = sut.view
        
        //Assert
        XCTAssertTrue(mockViewModel.getProductsCalled,"view load should call view model for products")
    }
    
    func testAfterHavingProducts_ShouldReloadCollectionView() {
        //Arrange
        let mockUICollectionView = MockUICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        sut.productsCollectionView = mockUICollectionView
        
        //Act
        sut.productsLoaded(with: .success(()))
        
        //Assert
        XCTAssertTrue(mockUICollectionView.reloadGotCalled,"CollectionView Reload method should be called")
    }
}
