//
//  NSDecimalNumber.swift
//  DishWashers
//
//  Created by Waheed Malik on 30/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

extension NSDecimalNumber {
    func toCurrency() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.roundingMode = .down
        numberFormatter.locale = Locale(identifier: "en_GB")
        return numberFormatter.string(from: self)!
    }
}
