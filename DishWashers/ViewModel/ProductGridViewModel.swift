//
//  ProductGridViewModel.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

// MARK:- VIEW DELEGATE PROTOCOL
protocol ProductGridViewModelViewDelegate: class {
    func productsLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol ProductGridViewModel {
    func getProducts()
    func numberOfProducts() -> Int
    func product(at index: Int) -> ProductViewModel?
    func photo(at index: Int, completionHandler: @escaping (Result<UIImage>) -> Void)
}

class ProductGridViewModelImpl: ProductGridViewModel  {
    // MARK: PRIVATE VARIABLES
    private let productManager: ProductManager
    private weak var viewDelegate: ProductGridViewModelViewDelegate!
    private var products: [Product]?
    
    // MARK: INITIALIZER
    init(productManager: ProductManager, viewDelegate: ProductGridViewModelViewDelegate) {
        self.productManager = productManager
        self.viewDelegate = viewDelegate
    }
    
    // MARK: PUBLIC METHODS
    func getProducts() {
        var vmResult: Result<Void>!
        productManager.loadProducts { [weak self] (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                self?.products = products
                vmResult = .success(())
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.productsLoaded(with: vmResult)
        }
    }
    
    func numberOfProducts() -> Int {
        guard let products = self.products else {
            return 0
        }
        return products.count
    }
    
    func product(at index: Int) -> ProductViewModel? {
        if let products = self.products, index < products.count {
            return ProductViewModelImpl(product: products[index])
        }
        return nil
    }
    
    func photo(at index: Int, completionHandler: @escaping (Result<UIImage>) -> Void) {
        if let products = self.products, index < products.count,
        let productImageUrl = products[index].imageUrlStr {
            productManager.loadPhoto(with: productImageUrl) { (result: Result<UIImage>) in
                completionHandler(result)
            }
        }
    }
}
