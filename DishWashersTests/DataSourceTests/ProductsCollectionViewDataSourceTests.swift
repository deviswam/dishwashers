//
//  ProductsCollectionViewDataSourceTests.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import DishWashers

extension ProductsCollectionViewDataSourceTests {
    class MockProductGridViewModel : ProductGridViewModel{
        var products: [Product]?
        func getProducts() {
            
        }
        
        func numberOfProducts() -> Int {
            return products!.count
        }
        
        func product(at index: Int) -> ProductViewModel? {
            return ProductViewModelImpl(product: products![index])
        }
        
        var getPhotoCalledForProductIndex: Int?
        var photoCompletionHandler: ((Result<UIImage>) -> Void)?
        func photo(at index: Int, completionHandler: @escaping (Result<UIImage>) -> Void) {
            getPhotoCalledForProductIndex = index
            self.photoCompletionHandler = completionHandler
        }
    }
    
    class MockCollectionView: UICollectionView {
        var cellGotDequeued = false
        override func dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        }
    }
    
    class MockProductCollectionViewCell : UICollectionViewCell, ProductCollectionViewCell {
        var configCellGotCalled = false
        var product : ProductViewModel?
        var productImage: UIImage?
        func configCell(with productViewModel: ProductViewModel) {
            configCellGotCalled = true
            self.product = productViewModel
        }
        func setProductImage(image: UIImage) {
            productImage = image
        }
    }
}

class ProductsCollectionViewDataSourceTests: XCTestCase {
    var sut: ProductsCollectionViewDataSource!
    var mockCollectionView: MockCollectionView!
    var mockViewModel: MockProductGridViewModel!
    
    override func setUp() {
        super.setUp()
        sut = ProductsCollectionViewDataSource()
        mockViewModel = MockProductGridViewModel()
        mockCollectionView = MockCollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        sut.viewModel = mockViewModel
        mockCollectionView.dataSource = sut
    }
    
    
    func testDataSourceHasProductGridViewModel() {
        // Assert
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil((sut.viewModel! as Any) is ProductGridViewModel)
    }
    
    func testNumberOfSections_IsOne() {
        //Arrange
        let noOfSections = sut.numberOfSections(in: mockCollectionView)
        //Assert
        XCTAssertEqual(noOfSections, 1,"CollectionView has only one section")
    }
    
    func testNumberOfItemsInTheSection_WithTwoProducts_ShouldReturnTwo() {
        //Arrange
        let products = [ProductImpl(id: 123), ProductImpl(id: 423)]
        mockViewModel.products = products

        //Act
        let noOfItemsInSectionZero = sut.collectionView(mockCollectionView, numberOfItemsInSection: 0)
        
        //Assert
        XCTAssertEqual(noOfItemsInSectionZero, 2, "Number of items in section 0 are 2")
    }
    
    func testCellForItem_ReturnsProductCollectionViewCell() {
        //Arrange
        let products = [ProductImpl(id: 123), ProductImpl(id: 423)]
        mockViewModel.products = products
        
        mockCollectionView.register(MockProductCollectionViewCell.self, forCellWithReuseIdentifier: "ProductCell")
        
        //Act
        let cell = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(cell is ProductCollectionViewCell,"Returned cell is of ProductCollectionViewCell type")
    }
    
    func testCellForItem_DequeuesCell() {
        //Arrange
        let products = [ProductImpl(id: 123), ProductImpl(id: 423)]
        mockViewModel.products = products
        
        mockCollectionView.register(MockProductCollectionViewCell.self, forCellWithReuseIdentifier: "ProductCell")
        
        //Act
        _ = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(mockCollectionView.cellGotDequeued,"CellForItem should be calling DequeueCell method")
    }
    
    func testConfigCell_GetsCalledFromCellForItem() {
        //Arrange
        let products = [ProductImpl(id: 123, title: "Bosch Dishwasher")]
        mockViewModel.products = products
        
        mockCollectionView.register(MockProductCollectionViewCell.self, forCellWithReuseIdentifier: "ProductCell")
        
        //Act
        let cell = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0)) as! MockProductCollectionViewCell
        
        //Assert
        XCTAssertTrue(cell.configCellGotCalled,"CellForItem should be calling ConfigCell method")
        XCTAssertTrue(cell.product?.title == products.first?.title, "Products should be same")
    }
    
    func testCellForItem_ShouldAskViewModelForPhotoOfAProduct() {
        //Arrange
        let products = [ProductImpl(id: 123, title: "Bosch Dishwasher"), ProductImpl(id: 423, title: "Bosch Dishwasher 2")]
        mockViewModel.products = products
        
        mockCollectionView.register(MockProductCollectionViewCell.self, forCellWithReuseIdentifier: "ProductCell")
        
        //Act
        _ = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 1, section: 0)) as! MockProductCollectionViewCell
        
        //Assert
        XCTAssertEqual(mockViewModel.getPhotoCalledForProductIndex!, 1,"Provider should be asking Manager for product photo")
    }
    
    func testCellForItem_AfterHavingPhotoOfAProduct_SetItOnCorrectCollectionViewCell() {
        //Arrange
        let product = ProductImpl(id: 123)
        mockViewModel.products = [product]
        let expectedPhoto = UIImage(named: "placeholder")!
        
        mockCollectionView.register(MockProductCollectionViewCell.self, forCellWithReuseIdentifier: "ProductCell")
        
        let cell = sut.collectionView(mockCollectionView, cellForItemAt: IndexPath(item: 0, section: 0)) as! MockProductCollectionViewCell
        
        //Act
        mockViewModel.photoCompletionHandler?(.success(expectedPhoto))
        
        //Assert
        XCTAssertNotNil(cell.productImage)
        XCTAssertEqual(cell.productImage!, expectedPhoto,"Cell should receive the expected product image")
    }
}
