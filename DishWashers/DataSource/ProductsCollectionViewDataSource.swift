//
//  ProductsCollectionViewDataSource.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ProductsCollectionViewDataSource: NSObject, UICollectionViewDataSource {
    var viewModel: ProductGridViewModel?
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfProducts()
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath)
        if let productCell = cell as? ProductCollectionViewCell, let productVM = viewModel?.product(at: indexPath.item) {
            productCell.configCell(with: productVM)
            self.loadPhoto(at: indexPath, onCell: productCell)
        }
        return cell
    }
    
    private func loadPhoto(at indexPath: IndexPath, onCell cell: ProductCollectionViewCell) {
        viewModel?.photo(at: indexPath.item, completionHandler: { (result: Result<UIImage>) in
            if case let Result.success(image) = result {
                cell.setProductImage(image: image)
            }
        })
    }
}
