IMPORTANT NOTES
===============

1. The project fully implements the product grid user story by showing collection of dishwashers.
2. iOS 9.0 onwards supported. Tested on all major iPad devices in both orientation. 
3. The project has been developed on latest xcode 9.4.1 with Swift 4.1 without any warnings or errors.
4. The app is following MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern. 
5. Project Test suite has 64 test cases covering all of the core application components. 
6. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
7. Please follow xcode console for any user info messages incase of issue with retriving data. Also, no retry option is provided to the user to request reloading of data. Please re-run the app if this happens.
8. This project is built on best software engineering practices. To name a few: SOLID principles, composition over inheritance, protocol oriented programming, loosely coupled architecture and TDD.

Thanks for your time.
