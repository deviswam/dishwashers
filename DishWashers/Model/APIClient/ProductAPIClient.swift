//
//  JohnLewisAPIClient.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

enum ProductAPIClientError: Error {
    case httpError
    case invalidDataError
    case dataDecodingError
}

protocol ProductAPIClient {
    func fetchProducts(completionHandler: @escaping (_ result: Result<[Product]>) -> Void)
    func fetchPhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class JohnLewisProductAPIClient: ProductAPIClient {
    // MARK: PRIVATE VARIABLES
    private let BASE_URL = "https://api.johnlewis.com/v1/products/search"
    private let API_Key = "Wu1Xqn3vNrd1p7hqkvB6hEu0G9OrsYGb"
    private let urlSession: URLSession!
    
    // MARK: PRIVATE DATA STRUCTURE
    private struct ProductsFeed: Decodable {
        let products: [ProductImpl]
    }
    
    // MARK: INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    // MARK: PUBLIC METHODS
    func fetchPhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        //print("WAM PHOTO URL:\(url.absoluteString)")
        guard let urlRequest = self.createURLRequest(from: photoUrl) else {
            return
        }
        self.getPhoto(with: urlRequest) { (result: Result<UIImage>) in
            completionHandler(result)
        }
    }
    
    func fetchProducts(completionHandler: @escaping (_ result: Result<[Product]>) -> Void) {
        let requestParams = ["q": "dishwasher",
                             "key": API_Key,
                             "pageSize": "20"]
        guard let urlRequest = self.createURLRequest(from: BASE_URL, with: requestParams) else {
            return
        }
        self.getProducts(with: urlRequest) { (result: Result<[Product]>) in
            completionHandler(result)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func createURLRequest(from urlString:String, with parameters:[String: String] = [:]) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: urlString)
        
        // append custom parameters
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        print("WAM URL:\(url.absoluteString)")
        
        // create request object
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        
        return request
    }
    
    private func getPhoto(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<UIImage>) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            var result: Result<UIImage>!
            if error != nil {
                result = .failure(ProductAPIClientError.httpError)
            } else if let data = data {
                let image = UIImage(data: data)
                if image == nil {
                    result = .failure(ProductAPIClientError.invalidDataError)
                } else {
                    result = .success(image!)
                }
            }
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    private func getProducts(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<[Product]>) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            var result: Result<[Product]>!
            
            if error != nil {
                result = .failure(ProductAPIClientError.httpError)
            } else if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let productsFeed = try decoder.decode(ProductsFeed.self, from: data)
                    result = .success(productsFeed.products)
                } catch {
                    result = .failure(ProductAPIClientError.dataDecodingError)
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
}
