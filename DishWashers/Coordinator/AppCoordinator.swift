//
//  AppCoordinator.swift
//  FoodRatings
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AppCoordinator : Coordinator {
    // MARK: PRIVATE VARIABLES
    private var window: UIWindow
    private var productGridCoordinator : ProductGridCoordinator?
    
    // MARK: INITIALIZER
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK: PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let rootNavController = storyboard.instantiateInitialViewController() as? UINavigationController {
            self.window.rootViewController = rootNavController
            self.showProductGrid(rootNavController: rootNavController)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func showProductGrid(rootNavController: UINavigationController) {
        productGridCoordinator = ProductGridCoordinator(navController: rootNavController)
        productGridCoordinator?.start()
    }
}
