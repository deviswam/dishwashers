//
//  Product.swift
//  DishWashers
//
//  Created by Waheed Malik on 28/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Product: Decodable {
    var id : Int { get }
    var title : String? { get }
    var currentPrice : NSDecimalNumber? { get }
    var imageUrlStr : String? { get }
}

struct ProductImpl: Product {
    let id : Int
    let title: String?
    let currentPrice: NSDecimalNumber?
    let imageUrlStr: String?
    
    init(id: Int, title: String? = nil, price: NSDecimalNumber? = nil, imageUrlStr: String? = nil) {
        self.id = id
        self.title = title
        self.currentPrice = price
        self.imageUrlStr = imageUrlStr
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "productId"
        case title
        case price
        case currentPrice = "now"
        case imageUrlStr = "image"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let productIdStr = try values.decode(String.self, forKey: .id)
        id = Int(productIdStr)!
        
        title = try values.decodeIfPresent(String.self, forKey: .title)
        
        var imageUrl: String? = nil
        if let decodedImageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrlStr) {
            imageUrl = "https:" + decodedImageUrl
        }
        imageUrlStr = imageUrl
        
        let priceValues = try? values.nestedContainer(keyedBy: CodingKeys.self, forKey: .price)
        let currentPriceStr = try priceValues?.decodeIfPresent(String.self, forKey: CodingKeys.currentPrice)
        currentPrice = NSDecimalNumber(string: currentPriceStr)
    }
}

func == (lhs: Product, rhs: Product) -> Bool {
    if lhs.id == rhs.id {
        return true
    }
    return false
}
