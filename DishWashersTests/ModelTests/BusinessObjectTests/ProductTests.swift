//
//  ProductTests.swift
//  DishWashers
//
//  Created by Waheed Malik on 28/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import DishWashers

class ProductTests: XCTestCase {
    func testConformanceToProductProtocol() {
        //Arrange
        let sut = ProductImpl(id: 123456)
        
        //Assert
        XCTAssertTrue((sut as Any) is Product, "Product should conforms to Product protocol")
    }
    
    func testConformanceToDecodableProtocol() {
        // Arrange
        let sut = ProductImpl(id: 123456)
        // Assert
        XCTAssert((sut as Any) is Decodable)
    }
    
    func testInit_productMustBeCreatedWithAnId() {
        //Arrange
        let sut = ProductImpl(id: 123456)
        
        //Assert
        XCTAssertEqual(sut.id,123456, "Product must have an id to get created")
    }
    
    func testInit_productShouldHaveATitle() {
        //Arrange
        let sut = ProductImpl(id: 12345, title:"Bosch dishwasher")
        
        //Assert
        XCTAssertEqual(sut.title,"Bosch dishwasher", "Product should have a title")
    }
    
    func testInit_ProductShouldHaveAPrice() {
        //Arrange
        let sut = ProductImpl(id: 12345, price:456.78)
        
        //Assert
        XCTAssertEqual(sut.currentPrice,456.78, "Product should have a price")
    }
    
    func testInit_ProductShouldHaveAImageUrlStr() {
        //Arrange
        let sut = ProductImpl(id: 12345, imageUrlStr: "http://sampleurl.com")
        
        //Assert
        XCTAssertEqual(sut.imageUrlStr, "http://sampleurl.com", "Product should have a image url")
    }
    
    func testProductsForEquality_withSameId_areSameProducts() {
        //Arrange
        let product1 = ProductImpl(id: 12345, price:456.78)
        let product2 = ProductImpl(id: 12345)
        
        //Assert
        XCTAssertTrue(product1 == product2, "Both are same product")
    }
    
    func testInit_withInvalidProductJSONData_shouldFailDecoding() {
        //Arrange
        let sut = try? JSONDecoder().decode(ProductImpl.self, from: Data())
        
        //Assert
        XCTAssertNil(sut, "Product should be created only with valid json data")
    }
    
    func testInit_withNoProductIdInJSONData_shouldFailProductCreation() {
        // Arrange
        let title = "Bosch SMS25AW00G Freestanding Dishwasher"
        let productData = """
        {
        "title" : "\(title)"
        }
        """
        
        let product = try? JSONDecoder().decode(ProductImpl.self, from: productData.data(using: .utf8)!)
        
        // Assert
        XCTAssertNil(product, "Product should be created only when id is present")
    }
    
    func testInit_withProductIdInJSONData_shouldSetProductId() {
        // Arrange
        let productId = 123
        let productData = """
        {
        "productId" : "\(productId)"
        }
        """
        
        let product = try? JSONDecoder().decode(ProductImpl.self, from: productData.data(using: .utf8)!)
        
        // Assert
        XCTAssertEqual(product?.id, productId)
    }
    
    func testInit_withProductTitleInJSONData_shouldSetProductTitle() {
        // Arrange
        let title = "Bosch SMS25AW00G Freestanding Dishwasher"
        let productData = """
        {
        "productId" : "123",
        "title" : "\(title)"
        }
        """
        
        let product = try? JSONDecoder().decode(ProductImpl.self, from: productData.data(using: .utf8)!)
        
        // Assert
        XCTAssertEqual(product?.title, title)
    }
    
    func testInit_withProductImageUrlInJSONData_shouldSetProductImageUrlStr() {
        // Arrange
        let imageUrl = "//johnlewis.scene7.com/is/image/JohnLewis/236888507?"
        let productData = """
        {
        "productId" : "123",
        "image" : "\(imageUrl)"
        }
        """
        
        let product = try? JSONDecoder().decode(ProductImpl.self, from: productData.data(using: .utf8)!)
        
        // Assert
        XCTAssertEqual(product?.imageUrlStr!, "https:" + imageUrl)
    }
    
    func testInit_withPriceInJSONData_shouldSetProductCurrentPrice() {
        // Arrange
        let currentPrice = 349.00
        let productData = """
        {
        "productId" : "123",
        "price" :
            {
                "now" : "\(currentPrice)"
            }
        }
        """
        
        let product = try? JSONDecoder().decode(ProductImpl.self, from: productData.data(using: .utf8)!)
        
        // Assert
        XCTAssertEqual(product?.currentPrice?.doubleValue, currentPrice)
    }
}
