//
//  ProductGridViewModelTests.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import DishWashers

extension ProductGridViewModelTests {
    class MockProductManager: ProductManager {
        var loadProductsCalled = false
        var completionHandler: ((Result<[Product]>) -> Void)?
        func loadProducts(completionHandler: @escaping (_ result: Result<[Product]>) -> Void) {
            self.loadProductsCalled = true
            self.completionHandler = completionHandler
        }
        func loadPhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        }
    }
    
    class MockProductGridViewModelViewDelegate: ProductGridViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var productGridViewModelResult: Result<Void>?
        
        func productsLoaded(with result: Result<Void>) {
            guard let asynExpectation = expectation else {
                XCTFail("MockProductGridViewModelViewDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            self.productGridViewModelResult = result
            asynExpectation.fulfill()
        }
    }
}

class ProductGridViewModelTests: XCTestCase {
    var sut: ProductGridViewModelImpl!
    var mockProductManager: MockProductManager!
    var mockViewDelegate: MockProductGridViewModelViewDelegate!
    
    override func setUp() {
        super.setUp()
        mockProductManager = MockProductManager()
        mockViewDelegate = MockProductGridViewModelViewDelegate()
        sut = ProductGridViewModelImpl(productManager: mockProductManager, viewDelegate: mockViewDelegate)
    }
    
    func testConformanceToProductGridViewModelProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is ProductGridViewModel, "ProductGridViewModel should conforms to ProductGridViewModel protocol")
    }
    
    func testGetProducts_shouldAskProductManagerForListOfProducts() {
        // Act
        sut.getProducts()
        // Assert
        XCTAssert(mockProductManager.loadProductsCalled)
    }
    
    func testGetProducts_whenProductsFound_raisesProductsLoadedDelegateCallbackWithSuccess() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getProducts()
        mockProductManager.completionHandler?(Result.success([ProductImpl(id: 123)]))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.productGridViewModelResult! {
            case .success():
                XCTAssert(true)
            default:
                XCTFail("View delegate should only receive success result when products found")
            }
        }
    }
    
    func testGetProducts_whenNoProductsFound_raisesProductsLoadedDelegateCallbackWithFailure() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getProducts()
        mockProductManager.completionHandler?(Result.failure(ProductManagerError.noProductsFoundError))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.productGridViewModelResult! {
            case .success():
                XCTFail("View delegate should receive failure with an error when NO products found")
            case .failure(let error):
                XCTAssertEqual(error as! ProductManagerError, ProductManagerError.noProductsFoundError)
            }
        }
    }
    
    func testNumberOfProducts_whenNoProductFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getProducts()
        mockProductManager.completionHandler?(Result.failure(ProductManagerError.noProductsFoundError))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfProducts(), 0)
        }
    }
    
    func testNumberOfProducts_whenOneProductFound_returnsOne() {
        // Arrange
        let products = [ProductImpl(id: 123)]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getProducts()
        mockProductManager.completionHandler?(Result.success(products))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfProducts(), 1)
        }
    }
    
    func testNumberOfProducts_whenFourProductsFound_returnsFour() {
        // Arrange
        let products = [ProductImpl(id: 123), ProductImpl(id: 453), ProductImpl(id: 563), ProductImpl(id: 367)]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getProducts()
        mockProductManager.completionHandler?(Result.success(products))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfProducts(), 4)
        }
    }
    
    func testProductAtIndex_returnsTheCorrectProductViewModelObject() {
        // Arrange
        let products = [ProductImpl(id: 123, title: "bosch dishwasher"), ProductImpl(id: 453, title: "cheap dishwasher")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getProducts()
        mockProductManager.completionHandler?(Result.success(products))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let product1 = self.sut.product(at: 0)
            let product2 = self.sut.product(at: 1)
            XCTAssertEqual(product1?.title, "bosch dishwasher")
            XCTAssertEqual(product2?.title, "cheap dishwasher")
        }
    }
    
    func testProductAtIndex_whenProductNotFoundOnPassedIndex_returnsNil() {
        // Arrange
        let products = [ProductImpl(id: 123, title: "bosch dishwasher"), ProductImpl(id: 453, title: "cheap dishwasher")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getProducts()
        mockProductManager.completionHandler?(Result.success(products))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let product = self.sut.product(at: 2)
            XCTAssertNil(product)
        }
    }
}
