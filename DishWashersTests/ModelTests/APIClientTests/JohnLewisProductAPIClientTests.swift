//
//  JohnLewisProductAPIClientTests.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import DishWashers

extension JohnLewisProductAPIClientTests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class JohnLewisProductAPIClientTests: XCTestCase {
    var mockURLSession: MockURLSession!
    var sut: JohnLewisProductAPIClient!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = JohnLewisProductAPIClient(urlSession: mockURLSession)
    }
    
    func testConformanceToProductAPIClient() {
        XCTAssertTrue((sut as AnyObject) is ProductAPIClient)
    }
    
    func testFetchProducts_shouldAskURLSessionForFetchingProductsData() {
        // Act
        sut.fetchProducts { (result: Result<[Product]>) in
            
        }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testFetchProducts_withValidJSONOfAProduct_shouldReturnAProductAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let productsAPIResponseInString = "{\"products\":[{" +
            "\"productId\":\"752073\"," +
            "\"title\":\"Bosch Dishwasher\"," +
            "\"price\":{" +
            "\"now\":\"329.00\"" +
            "}," +
            "\"image\": \"//testurl.com/image/123\"" +
        "}]}"
        
        let responseData = productsAPIResponseInString.data(using: .utf8)
        var receivedProduct : Product!
        var receivedError : ProductAPIClientError?
        
        //Act
        sut.fetchProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProduct = products.first
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedProduct.id, 752073, "Fetched and expected product id should be same")
            XCTAssertEqual(receivedProduct.title, "Bosch Dishwasher", "Fetched and expected product title should be same")
            XCTAssertEqual(receivedProduct.currentPrice, 329.00, "Fetched and expected product price should be same")
            XCTAssertEqual(receivedProduct.imageUrlStr, "https://testurl.com/image/123", "Fetched and expected product image url should be same")
            XCTAssertNil(receivedError, "Products method should return Nil as an Error along with Products object")
        }
    }
    
    func testFetchProducts_withValidJSONOfTwoProducts_shouldReturnTwoProductObjectsAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let productsAPIResponseInString = "{\"products\":[{" +
            "\"productId\":\"752073\"," +
            "\"title\":\"Bosch Dishwasher\"," +
            "\"price\":{" +
            "\"now\":\"329.00\"" +
            "}," +
            "\"image\": \"//testurl.com/image/123\"" +
            "},{" +
            "\"productId\":\"1913470\"," +
            "\"title\":\"Bosch Integrated Dishwasher\"," +
            "\"price\":{" +
            "\"now\":\"529.00\"" +
            "}," +
            "\"image\": \"//testurl.com/image/456\"" +
        "}]}"
        
        let responseData = productsAPIResponseInString.data(using: .utf8)
        var receivedProducts : [Product]?
        var receivedError : ProductAPIClientError?
        
        //Act
        sut.fetchProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedProducts?[0].id, 752073, "Fetched and expected product id should be same")
            XCTAssertEqual(receivedProducts?[0].title, "Bosch Dishwasher", "Fetched and expected product title should be same")
            XCTAssertEqual(receivedProducts?[0].currentPrice, 329.00, "Fetched and expected product price should be same")
            XCTAssertEqual(receivedProducts?[0].imageUrlStr, "https://testurl.com/image/123", "Fetched and expected product image url should be same")
            
            XCTAssertEqual(receivedProducts?[1].id, 1913470, "Fetched and expected product id should be same")
            XCTAssertEqual(receivedProducts?[1].title, "Bosch Integrated Dishwasher", "Fetched and expected product title should be same")
            XCTAssertEqual(receivedProducts?[1].currentPrice, 529.00, "Fetched and expected product price should be same")
            XCTAssertEqual(receivedProducts?[1].imageUrlStr, "https://testurl.com/image/456", "Fetched and expected product image url should be same")
            
            XCTAssertNil(receivedError, "Products method should return Nil as an Error along with Products object")
        }
    }
    
    func testFetchProducts_withInvalidJSON_shouldReturnDecodingErrorAndNilProducts() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let productsAPIResponseInString = "{\"product\":[{" +
            "\"productId\":\"752073\"," +
            "\"title\":\"Bosch Dishwasher\"," +
            "\"price\":{" +
            "\"now\":\"329.00\"" +
            "}," +
            "\"image\": \"testurl.com/image/123\"" +
        "}]}"
        
        let responseData = productsAPIResponseInString.data(using: .utf8)
        var receivedProducts : [Product]?
        var receivedError : ProductAPIClientError?
        
        //Act
        sut.fetchProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError! == ProductAPIClientError.dataDecodingError, "Invalid products JSON should return decoding Error")
            XCTAssertNil(receivedProducts, "Invalid Products JSON should return products array as Nil")
        }
    }
    
    func testFetchProducts_withEmptyData_shouldReturnDecodingErrorAndNilProducts() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        var receivedProducts : [Product]?
        var receivedError : ProductAPIClientError?
        
        //Act
        sut.fetchProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(Data(), nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError! == ProductAPIClientError.dataDecodingError, "Empty Data() should return decoding Error")
            XCTAssertNil(receivedProducts, "Invalid Products JSON should return products array as Nil")
        }
    }
    
    func testFetchProducts_withMissingJSONConstructs_shouldReturnDecodingErrorAndNilProducts() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let productsAPIResponseInString = "{\"products\":{" +
            "\"productId\":\"752073\"," +
            "\"title\":\"Bosch Dishwasher\"," +
            "\"price\":{" +
            "\"now\":\"329.00\"" +
            "}," +
            "\"image\": \"testurl.com/image/123\"" +
        "}]}"
        
        let responseData = productsAPIResponseInString.data(using: .utf8)
        var receivedProducts : [Product]?
        var receivedError : ProductAPIClientError?
        
        //Act
        sut.fetchProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError == ProductAPIClientError.dataDecodingError, "Missing required constructs in JSON should return decoding Error")
            XCTAssertNil(receivedProducts, "Missing required constructs in JSON should return products array as Nil")
        }
    }
    
    func testFetchProducts_withHttpError_shouldReturnHttpErrorAndNilProducts() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let productsAPIResponseInString = "{\"products\":[{" +
            "\"productId\":\"752073\"," +
            "\"title\":\"Bosch Dishwasher\"," +
            "\"price\":{" +
            "\"now\":\"329.00\"" +
            "}," +
            "\"image\": \"testurl.com/image/123\"" +
        "}]}"
        
        let responseData = productsAPIResponseInString.data(using: .utf8)
        var receivedProducts : [Product]?
        let expectedError = ProductAPIClientError.httpError
        var receivedError : ProductAPIClientError?
        
        //Act
        sut.fetchProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(responseData, nil, expectedError)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedError == expectedError, "Session HTTP Error should return a http error")
            XCTAssertNil(receivedProducts, "Products should return products array as Nil when httpError occurs")
        }
    }
    
    func testFetchProducts_shouldCallResumeMethodOfDataTaskToFireupRequest() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        //Act
        sut.fetchProducts { (result: Result<[Product]>) in
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(Data(), nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(self.mockURLSession.dataTask.resumeGotCalled, "Fetch Products method should asks Resume method of Data Task to trigger request")
        }
    }
    
    func testFetchPhoto_withPhotoUrl_shouldAskURLSessionForFetchingAProductPhoto() {
        //Act
        sut.fetchPhoto(with: "https://sampleurl.com") { (result: Result<UIImage>) in
            
        }
        
        //Assert
        XCTAssertTrue(mockURLSession.dataTaskRequestMethodCalled, "Photo method should ask urlSession to create data task request")
    }
    
    func testFetchPhoto_shouldFetchPhotoFromCorrectPhotoURL() {
        //Arrange
        let expectedPhotoURL = "https://sampleurl.com?"
        
        //Act
        sut.fetchPhoto(with: "https://sampleurl.com") { (result: Result<UIImage>) in
            
        }
        
        //Assert
        XCTAssertEqual(mockURLSession.urlRequest?.url?.absoluteString, expectedPhotoURL , "Photo method should fetch photo from correct url")
    }
    
    func testFetchPhoto_withValidPhotoData_shouldReturnAPhotoAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let expectedImage = UIImage(named: "placeholder")
        let expectedImageData = UIImagePNGRepresentation(expectedImage!)
        var receivedImageData : Data?
        var receivedError : ProductAPIClientError?
        
        //Act
        sut.fetchPhoto(with: "https://sampleurl.com") { (result: Result<UIImage>) in
            switch result {
            case .success(let photo):
                receivedImageData = UIImagePNGRepresentation(photo)
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(expectedImageData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedImageData!, expectedImageData!, "Fetched and expected image should be same")
            XCTAssertNil(receivedError)
        }
    }
    
    func testFetchPhoto_withInValidPhotoData_shouldReturnInvalidDataErrorAndNilPhoto() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let expectedImageData = Data()
        let expectedError = ProductAPIClientError.invalidDataError
        var receivedError : ProductAPIClientError?
        var receivedImageData : Data?
        
        //Act
        sut.fetchPhoto(with: "https://sampleurl.com") { (result: Result<UIImage>) in
            switch result {
            case .success(let photo):
                receivedImageData = UIImagePNGRepresentation(photo)
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(expectedImageData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedError!, expectedError, "received and expected erros should be same")
            XCTAssertNil(receivedImageData)
        }
    }
    
    func testFetchPhoto_withHttpError_shouldReturnHttpErrorAndNilPhotos() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let expectedImageData = Data()
        let expectedError = ProductAPIClientError.httpError
        var receivedError : ProductAPIClientError?
        var receivedImageData : Data?
        
        //Act
        sut.fetchPhoto(with: "https://sampleurl.com") { (result: Result<UIImage>) in
            switch result {
            case .success(let photo):
                receivedImageData = UIImagePNGRepresentation(photo)
            case .failure(let error):
                receivedError = error as? ProductAPIClientError
            }
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(expectedImageData, nil, expectedError)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertEqual(receivedError!, expectedError, "received and expected erros should be same")
            XCTAssertNil(receivedImageData)
        }
    }
    
    func testFetchPhoto_shouldCallResumeMethodOfDataTaskToFireupRequest() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        //Act
        sut.fetchPhoto(with: "https://sampleurl.com") { (result: Result<UIImage>) in
            asynExpectation.fulfill()
        }
        mockURLSession.completionHandler!(Data(), nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(self.mockURLSession.dataTask.resumeGotCalled, "fetch Photo method should asks Resume method of Data Task to trigger request")
        }
    }
}
