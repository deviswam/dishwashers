//
//  ProductCollectionViewCellTests.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import DishWashers

extension ProductCollectionViewCellTests {
    class FakeDataSource: NSObject, UICollectionViewDataSource {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath)
            return cell
        }
    }
    
    class FakeProductViewModel: ProductViewModel {
        var title: String { return "Bosch Dishwasher"}
        var price: String { return "£456.90"}
    }
}

class ProductCollectionViewCellTests: XCTestCase {
    let fakeDataSource = FakeDataSource()
    var collectionView : UICollectionView!
    var cell : ProductCollectionViewCellImpl!
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let productGridVC = storyBoard.instantiateViewController(withIdentifier: "ProductGridVC") as! ProductGridVC
        productGridVC.productsCollectionViewDataSource = fakeDataSource
        UIApplication.shared.keyWindow?.rootViewController = productGridVC
        _ = productGridVC.view
        collectionView = productGridVC.productsCollectionView
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: IndexPath(row: 0, section: 0)) as! ProductCollectionViewCellImpl
    }
    
    func testProductCell_HasTitleLabel() {
        // Assert
        XCTAssertNotNil(cell.productTitleLabel)
    }
    
    func testProductCell_HasPriceLabel() {
        // Assert
        XCTAssertNotNil(cell.productPriceLabel)
    }
    
    func testProductCell_HasProductImageView() {
        // Assert
        XCTAssertNotNil(cell.productImageView)
    }
    
    func testConfigCell_SetTitleAndPriceOnRespectiveLabels() {
        //Act
        cell.configCell(with: FakeProductViewModel())
        
        //Assert
        XCTAssertEqual(cell.productTitleLabel.text!, "Bosch Dishwasher")
        XCTAssertEqual(cell.productPriceLabel.text!, "£456.90")
    }
}
