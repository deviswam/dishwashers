//
//  ProductCollectionViewCell.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol ProductCollectionViewCell {
    func configCell(with productViewModel: ProductViewModel)
    func setProductImage(image: UIImage)
}

class ProductCollectionViewCellImpl: UICollectionViewCell, ProductCollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    
    func configCell(with productViewModel: ProductViewModel) {
        productTitleLabel.text = productViewModel.title
        productPriceLabel.text = productViewModel.price
    }
    
    func setProductImage(image: UIImage) {
        self.productImageView.image = image
    }
    
    override func prepareForReuse() {
        self.productImageView.image = UIImage(named: "placeholder")
    }
}
