//
//  ProductViewModel.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol ProductViewModel {
    var title: String { get }
    var price: String { get }
}

class ProductViewModelImpl: ProductViewModel {
    var title: String = ""
    var price: String = ""
    init(product: Product) {
        if let title = product.title {
            self.title = title
        }
        if let price = product.currentPrice {
            self.price = price.toCurrency()
        }
    }
}
