//
//  ProductManager.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

enum ProductManagerError : Error {
    case noProductsFoundError
    case noPhotoFoundError
    case connectionError
    
    func userFriendlyMessage() -> String {
        switch self {
        case .noProductsFoundError:
            return "No Products Found"
        case .noPhotoFoundError:
            return "No Photo Found"
        case .connectionError:
            return "Connection Error"
        }
    }
}

//MARK: INTERNAL ENUM
enum ProductManagerMethodType {
    case products
    case photo
}

protocol ProductManager {
    func loadProducts(completionHandler: @escaping (_ result: Result<[Product]>) -> Void)
    func loadPhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class ProductManagerImpl: ProductManager {
    // MARK: PRIVATE VARIABLES
    private let apiClient: ProductAPIClient
    private let imageStore: ImageStore
    
    // MARK: INITIALIZER
    init(apiClient: ProductAPIClient, imageStore: ImageStore) {
        self.apiClient = apiClient
        self.imageStore = imageStore
    }
    
    // MARK: PUBLIC METHODS
    func loadProducts(completionHandler: @escaping (_ result: Result<[Product]>) -> Void) {
        apiClient.fetchProducts { [unowned self] (result: Result<[Product]>) in
            var mResult = result
            if case let Result.failure(error) = result {
                mResult = .failure(self.productManagerError(from: error as! ProductAPIClientError, for: .products))
            }
            completionHandler(mResult)
        }
    }
    
    func loadPhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        imageStore.getImage(with: photoUrl) { [unowned self] (result: Result<UIImage>) in
            var mResult = result
            if case let Result.failure(error) = result {
                mResult = .failure(self.productManagerError(from: error as! ProductAPIClientError, for: .photo))
            }
            completionHandler(mResult)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func productManagerError(from apiError: ProductAPIClientError, for methodType: ProductManagerMethodType) -> ProductManagerError {
        switch apiError {
        case .dataDecodingError:
            return ProductManagerError.noProductsFoundError
        case .invalidDataError:
            switch methodType {
            case .products:
                return ProductManagerError.noProductsFoundError
            case .photo:
                return ProductManagerError.noPhotoFoundError
            }
        case .httpError:
            return ProductManagerError.connectionError
        }
    }
}
