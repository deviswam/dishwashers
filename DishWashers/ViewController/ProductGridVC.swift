//
//  ProductGridVC.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ProductGridVC: UIViewController {

    @IBOutlet weak var productsCollectionView: UICollectionView!
    var productsCollectionViewDataSource: UICollectionViewDataSource?
    var viewModel: ProductGridViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productsCollectionView.dataSource = productsCollectionViewDataSource
        
        // get products list
        viewModel?.getProducts()
    }
}

extension ProductGridVC: ProductGridViewModelViewDelegate {
    func productsLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.productsCollectionView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch dishwashers list")
        }
    }
}
