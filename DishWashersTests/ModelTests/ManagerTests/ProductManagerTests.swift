//
//  ProductManagerTests.swift
//  DishWashers
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import DishWashers

extension ProductManagerTests {
    class MockProductAPIClient: ProductAPIClient {
        var fetchProductsCalled = false
        var completionHandler: ((Result<[Product]>) -> Void)?
        func fetchProducts(completionHandler: @escaping (_ result: Result<[Product]>) -> Void) {
            fetchProductsCalled = true
            self.completionHandler = completionHandler
        }
        func fetchPhoto(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        }
    }
    
    class MockImageStore: ImageStore {
        var getImageCalledWithImageUrl: String!
        var completionHandler: ((Result<UIImage>) -> Void)?
        func getImage(with imageUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
            getImageCalledWithImageUrl = imageUrl
            self.completionHandler = completionHandler
        }
    }
}

class ProductManagerTests: XCTestCase {
    var mockProductAPIClient: MockProductAPIClient!
    var mockImageStore: MockImageStore!
    var sut: ProductManagerImpl!
    
    override func setUp() {
        super.setUp()
        mockProductAPIClient = MockProductAPIClient()
        mockImageStore = MockImageStore()
        sut = ProductManagerImpl(apiClient: mockProductAPIClient, imageStore: mockImageStore)
    }
    
    func testConformanceToProductManagerProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is ProductManager, "ProductManager should conforms to ProductManager protocol")
    }
    
    func testLoadProducts_asksProductAPIClientToFetchProducts() {
        // Act
        sut.loadProducts { (result: Result<[Product]>) in
            
        }
        
        // Assert
        XCTAssert(mockProductAPIClient.fetchProductsCalled)
    }
    
    func testLoadProducts_shouldReceiveProductsWithNilError_whenAPIClientFoundProducts() {
        // Arrange
        var receivedProducts : [Product]?
        var receivedError : Error?
        let expectedProducts = [ProductImpl(id: 1234)]
        
        // Act
        sut.loadProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error
            }
        }
        
        mockProductAPIClient.completionHandler?(.success(expectedProducts))
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedProducts)
        XCTAssert(expectedProducts.first! == receivedProducts!.first!)
    }
    
    func testLoadProducts_shouldReceiveNoProductsFoundError_whenAPIClientHasInvalidDataError() {
        // Arrange
        var receivedProducts : [Product]?
        var receivedError : Error?
        
        // Act
        sut.loadProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error
            }
        }
        mockProductAPIClient.completionHandler?(.failure(ProductAPIClientError.invalidDataError))
        
        //Assert
        XCTAssertEqual(receivedError as! ProductManagerError, ProductManagerError.noProductsFoundError, "Should receive NoProductsFoundError when underlying api has InvalidDataError")
        XCTAssertNil(receivedProducts, "No products are returned when error occured")
    }
    
    func testLoadProducts_shouldReceiveNoProductsFoundError_whenAPIClientHasDataDecodingError() {
        // Arrange
        var receivedProducts : [Product]?
        var receivedError : Error?
        
        // Act
        sut.loadProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error
            }
        }
        mockProductAPIClient.completionHandler?(.failure(ProductAPIClientError.dataDecodingError))
        
        //Assert
        XCTAssertEqual(receivedError as! ProductManagerError, ProductManagerError.noProductsFoundError, "Should receive NoProductsFoundError when underlying api has DataSerializationError")
        XCTAssertNil(receivedProducts, "No products are returned when error occured")
    }
    
    func testProducts_shouldReceiveConnectionError_whenAPIClientHasHttpError() {
        //Arrange
        var receivedProducts : [Product]?
        var receivedError : Error?
        
        // Act
        sut.loadProducts { (result: Result<[Product]>) in
            switch result {
            case .success(let products):
                receivedProducts = products
            case .failure(let error):
                receivedError = error
            }
        }
        mockProductAPIClient.completionHandler?(.failure(ProductAPIClientError.httpError))
        
        //Assert
        XCTAssertEqual(receivedError as! ProductManagerError, ProductManagerError.connectionError, "Should receive ConnectionError when underlying api has HttpError")
        XCTAssertNil(receivedProducts, "No products are returned when error occured")
    }
    
    func testLoadPhoto_shouldAskImageStoreToFetchAProductPhoto() {
        //Act
        sut.loadPhoto(with: "https://photourl.com") { (result: Result<UIImage>) in
            
        }
        //Assert
        XCTAssertEqual(mockImageStore.getImageCalledWithImageUrl, "https://photourl.com", "ProductManager should ask ImageStore to fetch a product photo")
    }
    
    func testLoadPhoto_shouldFetchAProductPhotoWithNilError() {
        //Arrange
        let expectedPhoto = UIImage(named: "placeholder")!
        var receivedPhoto : UIImage?
        var receivedError : ProductManagerError?
        
        //Act
        sut.loadPhoto(with: "https://photourl.com") { (result: Result<UIImage>) in
            switch result {
            case .success(let image):
                receivedPhoto = image
            case .failure(let error):
                receivedError = error as? ProductManagerError
            }
        }
        mockImageStore.completionHandler?(.success(expectedPhoto))
        
        //Assert
        XCTAssertEqual(expectedPhoto, receivedPhoto, "Fetched and expected photos should be same")
        XCTAssertNil(receivedError, "No error returned when photo is returned")
    }
    
    func testLoadPhoto_shouldReceiveNoPhotoFoundError_whenStoreHasInvalidDataError() {
        var receivedPhoto : UIImage?
        var receivedError : ProductManagerError?
        
        //Act
        sut.loadPhoto(with: "https://photourl.com") { (result: Result<UIImage>) in
            switch result {
            case .success(let image):
                receivedPhoto = image
            case .failure(let error):
                receivedError = error as? ProductManagerError
            }
        }
        mockImageStore.completionHandler?(.failure(ProductAPIClientError.invalidDataError))
        
        //Assert
        XCTAssertEqual(receivedError, ProductManagerError.noPhotoFoundError, "Should receive NoPhotoFoundError when underlying image store has InvalidDataError")
        XCTAssertNil(receivedPhoto, "No photo is returned when error occured")
    }
    
    func testLoadPhoto_shouldReceiveConnectionError_whenStoreHasHttpError() {
        //Arrange
        var receivedPhoto : UIImage?
        var receivedError : ProductManagerError?
        
        //Act
        sut.loadPhoto(with: "https://photourl.com") { (result: Result<UIImage>) in
            switch result {
            case .success(let image):
                receivedPhoto = image
            case .failure(let error):
                receivedError = error as? ProductManagerError
            }
        }
        mockImageStore.completionHandler?(.failure(ProductAPIClientError.httpError))
        
        //Assert
        XCTAssertEqual(receivedError, ProductManagerError.connectionError, "Should receive ConnectionError when underlying image store has HttpError")
        XCTAssertNil(receivedPhoto, "No photo is returned when error occurs")
    }
}
