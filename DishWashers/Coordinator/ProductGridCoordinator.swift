//
//  ProductGridCoordinator.swift
//  FoodRatings
//
//  Created by Waheed Malik on 29/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ProductGridCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    fileprivate var navigationController: UINavigationController
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        if let productGridVC = navigationController.topViewController as? ProductGridVC {
            let productAPIClient = JohnLewisProductAPIClient()
            let imageStore = ImageStoreImpl(apiClient: productAPIClient)
            let productManager = ProductManagerImpl(apiClient: productAPIClient, imageStore: imageStore)
            let productsCollectionViewDataSource = ProductsCollectionViewDataSource()
            let productGridViewModel = ProductGridViewModelImpl(productManager: productManager, viewDelegate: productGridVC)
            
            productGridVC.viewModel = productGridViewModel
            productGridVC.productsCollectionViewDataSource = productsCollectionViewDataSource
            productsCollectionViewDataSource.viewModel = productGridViewModel
        }
    }
}
